# vagrant-box

The project is used to create Vagrant Boxes on regular basis. Currently, 
boxes based on Debian GNU/Linux with Docker CE installed are provided.

* [tomaskadlec/debian-stretch](https://app.vagrantup.com/tomaskadlec/boxes/debian-stretch)

Please refer to [vagrant-box/README](roles/vagrant-box/README.md) for furher
information how boxes are built.

## License

The project is licensed under the [MIT License](LICENSE).

