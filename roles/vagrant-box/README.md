# vagrant-box

The role automates creating Vagrant boxes used for development purposes. It allows to build boxes of
various Debian GNU/Linux releases.

Each box is represented as an Ansible host. Such approach allows to easily customize the process
for each image while sharing most of the configuration amongst all images.

A host called _builder_ is required to build boxes. It may be your computer or you can build boxes on
a remote machine. Refer to [Usage](#Usage) section for further information.

TBD More detailed description

## Configuration

Default configuration is stored in [defaults/main.yml]. Please, refer to it for default values.
It may be customized per groups and hosts in your inventory. Main configuration related to preseeding
is `preseed` object.

`preseed.dir` build directory - temporary files and resulting artifacts are kept there

`preseed.disk` disk related configuration

`preseed.disk.device` device to use (defaults to `/dev/sda`)

`preseed.domain` domain name for the host

`preseed.headless` run VirtualBox in headless mode 

`preseed.kernel_args` kernel arguments for installer kernel

`preseed.pressed_args` installer arguments (kernel cmd line after `---`)

`preseed.packer` if set to `yes` builds a Vagrant Box using custom ISO image

`preseed.suite` suite to install (e.g. `stretch`, `buster`)

`preseed.user` a user to create (defaults to `debian`) 

### User accounts

User `root` cannot login locally and remotely. There is a user called `preseed.user` that is allowed to login. 
Password is the same as login name (i.e. `preseed.user`). Vagrant insecure SSH key is configured as authorized key (it 
is replaced on `vagrant up` with a new secure key).

### Other variables

Preseed also utilizes several other variables which are documented here.

`inventory_hostname`
    Value is used as hostname.

## Usage

This section is divided into two parts. The first part [Requirements](#Requirements) descrbe how to configure 
the _builder_. The second part [Creating boxes](#Creating_boxes) describes how to create Vagrant boxes.

## Requirements

In order to use the role _builder_ must be set up properly. You can use [requirements.yml] which contains 
a set of tasks that help you with configuring the _builder_. Just include it to your playbooks. It is
possible to provision localhost (e.g. ansible controller)

Configure localhost:

```
-   name: vagrant-box - install dependencies
    hosts: [localhost]
    connection: local
    become: yes
    tasks:
        -   include: roles/vagrant-box/requirements.yml
```

Configure remote host:

```
-   name: vagrant-box - install dependencies
    hosts: [vagrant-box-builders]
    become: yes
    tasks:
        -   include: roles/vagrant-box/requirements.yml
```

## Creating boxes

Include the role in your play to use it. Superuser privileges are not required. Don't forget to specify
a machine (`localhost` or a machine from inventory) tasks shall be delegated to. `gather_facts` must be 
set to `no`. You can limit parallelism using `serial`.

```
-   name: vagrant-box - create boxes
    hosts: [vagrant-boxes]
    gather_facts: no
    serial: 1
    roles:
        -   vagrant-box
            delegate_to: FIXME
```

Run you playbook. You can use `-l` (`--limit`) to limit boxes that will be created.

```
ansible-playbook -l debian-stretch vagrant-box.yml
```

[Vagrant Cloud]: https://app.vagrantup.com/tomaskadlec

Boxes are uploaded to [Vagrant Cloud]. You must provide two configuration options to actually publish 
a box:

* `preseed.box` is a name of the box, e.g. `tomaskadlec/debian-stretch`.
* `preseed.access_token` is an access token that is authorized to upload to specified box. By default,
  the role looks for environment variable `VAGRANT_CLOUD_ACCESS_TOKEN` and uses it's value.

Version of the box is generated automatically and it is simply current date formmated as `%Y%m%d`.
